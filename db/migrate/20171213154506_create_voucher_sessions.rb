class CreateVoucherSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :voucher_sessions do |t|
      t.integer :employee_id, foreign_key: true, index: true

      t.timestamps
    end
  end
end
