class AddVoucherSessionIdToVoucherAmounts < ActiveRecord::Migration[5.1]
  def change
  	add_column :voucher_amounts, :voucher_session_id, :integer, foreign_key: true
  end
end
