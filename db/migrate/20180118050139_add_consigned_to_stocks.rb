class AddConsignedToStocks < ActiveRecord::Migration[5.1]
  def change
  	add_column :stocks, :consigned, :boolean, default: false
  end
end
