class AddDateToInterestPrograms < ActiveRecord::Migration[5.1]
  def change
    add_column :interest_programs, :date, :datetime
  end
end
