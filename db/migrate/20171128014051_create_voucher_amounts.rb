class CreateVoucherAmounts < ActiveRecord::Migration[5.1]
  def change
    create_table :voucher_amounts do |t|
      t.belongs_to :voucher, foreign_key: true
      t.belongs_to :credit_account, foreign_key: {to_table: :accounts}
      t.belongs_to :debit_account, foreign_key: {to_table: :accounts}
      t.decimal :amount, default: 0
      t.text :description
      t.integer :amount_type

      t.timestamps
    end
  end
end
