class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.references :line_item, foreign_key: true, index:true
      t.decimal :amount, default: 0

      t.timestamps
    end
  end
end
