class CreateVouchers < ActiveRecord::Migration[5.1]
  def change
    create_table :vouchers do |t|
      t.datetime :date
      t.text :description
      t.integer :employee_id
      t.integer :commercial_document_id
      t.string :commercial_document_type

      t.timestamps
    end
    add_index :vouchers, :employee_id
    add_index :vouchers, :commercial_document_id
    add_index :vouchers, :commercial_document_type
  end
end
