class AddAccountsReceivableAccountToPrograms < ActiveRecord::Migration[5.1]
  def change
    add_reference :programs, :accounts_receivable_account, foreign_key: { to_table: :accounts }
    add_reference :programs, :accounts_receivable_interest_account, foreign_key: { to_table: :accounts }

  end
end
