class AddVoucherToEntries < ActiveRecord::Migration[5.1]
  def change
  	add_column :entries, :voucher_id, :integer, index: true, foreign_key: true
  end
end
