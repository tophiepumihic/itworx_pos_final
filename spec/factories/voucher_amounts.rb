FactoryGirl.define do
  factory :voucher_amount do
    voucher nil
    credit_account nil
    debit_account nil
    amount "9.99"
    description ""
  end
end
