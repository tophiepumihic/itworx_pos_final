FactoryGirl.define do
  factory :service_charge do
    description "MyString"
    unit "MyString"
    quantity "9.99"
    amount "9.99"
    charge_type 1
    order_id 1
  end
end
