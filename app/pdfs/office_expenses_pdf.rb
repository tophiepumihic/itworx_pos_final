class OfficeExpensesPdf < Prawn::Document
  TABLE_WIDTHS = [90, 60, 240, 80, 82]
  def initialize(office_expenses, from_date, to_date, view_context)
    super(margin: 30, page_size: [612, 1008], page_layout: :portrait)
    @orders = office_expenses
    @from_date = from_date
    @to_date = to_date
    @view_context = view_context
    heading
    display_expenses_table
    footer_for_itworx
  end
  def price(number)
    @view_context.number_to_currency(number, :unit => "P ")
  end
  def heading
    text "#{Business.last.try(:name)}", size: 11, align: :center, style: :bold
    text "#{Business.last.address}", size: 9, align: :center
    text "TIN: #{Business.last.try(:tin)}", size: 9, align: :center
    text "#{Business.last.try(:mobile_number)}  #{Business.last.try(:email)}", size: 8, align: :center
    move_down 10
    text "OFFICE EXPENSES REPORT", size: 11, align: :center, style: :bold
    move_down 1
    if @from_date.strftime('%B %e, %Y') == @to_date.strftime('%B %e, %Y')
      text "#{@from_date.strftime('%B %e, %Y')}", align: :center, size: 10
    else
      text "#{@from_date.strftime('%B %e, %Y')} - #{@to_date.strftime('%B %e, %Y')}", align: :center, size: 10
    end
    stroke_horizontal_rule
    move_down 2
    table(total_expenses_data, header: true, cell_style: { size: 10, font: "Helvetica", :padding => [1,4,1,4]}, column_widths: [150, 100, 100, 100, 100]) do
      cells.borders = []
      row(0).font_style = :bold
    end
    move_down 2
    stroke_horizontal_rule
    move_down 5
  end
  def total_expenses_data
    [["Total Expenses: ", price(Order.office_expenses.where(date: @from_date.yesterday.end_of_day..@to_date.end_of_day).sum(&:total_amount_without_discount)), "", "", ""]]
  end

  def display_expenses_table
    move_down 5
    if @orders.blank?
      text "No transactions.", align: :center
    else
      header = [["DATE", "QTY", "PRODUCT", "PRICE", "AMOUNT"]]
      table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        row(0).font_style = :bold
        column(1).align = :right
        column(2).align = :center
        column(4).align = :right
        column(5).align = :right
      end
      stroke_horizontal_rule
      move_down 5
      @orders.each do |order|
        header = ["", "", "", "", ""]
        footer = ["", "", "", "", ""]
        move_down 3
        text "Invoice Number: #{order.invoice_number.number}", size: 9, style: :bold
        line_items_data = order.line_items.map { |e| [
          e.created_at.strftime("%B %e, %Y"), 
          "#{e.quantity.to_i} #{e.stock.product.unit}", 
          e.stock.product.name_and_description, 
          price(e.unit_price), 
          price(e.total_price) 
        ]}

        table_data = [header, *line_items_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", align: :center, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = [:top]
          row(0).font_style = :bold
          row(0).align = :center
          column(1).align = :right
          column(2).align = :center
          column(4).align = :right
          column(5).align = :right
        end
      end
    end
    move_down 10
    stroke_horizontal_rule
  end

  def footer_for_itworx
    bounding_box([10, 5], :width => 500, :height => 110) do
      stroke_horizontal_rule
      move_down 5
      
    end
  end
end
