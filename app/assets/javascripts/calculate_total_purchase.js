function calculateTotalPurchase() {
  var quantity = document.getElementById('stock_quantity').value;
  var unitCost = document.getElementById('stock_unit_cost').value;

  var totalCost = document.getElementById('stock_total_cost');
  var myResult = quantity * unitCost;
  totalCost.value = myResult;
}
