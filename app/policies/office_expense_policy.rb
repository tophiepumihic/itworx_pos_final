class OfficeExpensePolicy < ApplicationPolicy
  def initialize(employee, office_expense)
    @employee = employee
    @office_expense = office_expense
  end
  def index?
    @employee.proprietor? || @employee.cashier? || @employee.developer?
  end
  def new?
  	@employee.proprietor? || @employee.cashier? || @employee.developer?
  end
end
