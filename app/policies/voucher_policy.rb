class VoucherPolicy < ApplicationPolicy
  def initialize(employee, voucher)
    @employee = employee
    @voucher = voucher
  end
  def index?
    @employee.bookkeeper? || @employee.proprietor? || @employee.developer?
  end
  def new?
    create?
  end
  def create?
    @employee.bookkeeper? || @employee.proprietor? || @employee.developer?
  end
end
