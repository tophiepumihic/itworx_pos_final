class StockPolicy < ApplicationPolicy
  def initialize(employee, stock)
    @employee = employee
    @stock = stock
  end

  def index?
    @employee.stock_custodian? || @employee.proprietor? || @employee.developer?
  end
end
