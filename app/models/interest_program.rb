class InterestProgram < ApplicationRecord
	belongs_to :line_item
	belongs_to :order

	def total_amount
		all.sum(:amount)
	end
end
