class Program < ApplicationRecord
	has_many :products
	has_many :line_items
	has_many :program_subscriptions
	has_many :customers, through: :program_subscriptions
	belongs_to :accounts_receivable_account, class_name: "Accounting::Account"
	belongs_to :accounts_receivable_interest_account, class_name: "Accounting::Account"

	validates :accounts_receivable_account_id, presence: true
	validates :accounts_receivable_interest_account_id, presence: true
	def total_amounts_for(order)
		total =[]
		self.products.each do |product| 
		  order.line_items.each do |line_item|
		  	if line_item.stock.product_id == product.id 
		  		total << line_item.total_cost 
		  	end 
		  end 
		end
		total.sum
	end

end
