class VoucherSession < ApplicationRecord
	belongs_to :employee
	has_many :voucher_amounts, dependent: :destroy
end
