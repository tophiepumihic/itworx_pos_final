class Order < ApplicationRecord
  include PgSearch
  pg_search_scope :text_search, :against => [:reference_number],
                  :associated_against => {:customer => [:full_name], :invoice_number => [:number]}

  has_one :official_receipt, as: :receiptable
  has_one :invoice_number, dependent: :destroy
  has_one :entry, class_name: "Accounting::Entry", as: :commercial_document, dependent: :destroy

  belongs_to :employee, foreign_key: 'employee_id'
  belongs_to :customer, foreign_key: 'customer_id'
  enum pay_type:[:cash, :credit]
  enum order_type: [:retail, :wholesale, :catering, :office_expense]
  enum payment_status: [:paid, :unpaid]
  enum delivery_type: [:pick_up, :deliver, :to_go]
  has_many :line_items, dependent: :destroy
  has_many :service_charges, dependent: :destroy
  has_many :stocks, through: :line_items
  has_one :discount, dependent: :destroy

  scope :sold_on, lambda {|start_date, end_date| where("date >= ? AND date <= ?", start_date, end_date )}
  belongs_to :tax
  before_save :set_date, :set_customer
  accepts_nested_attributes_for :discount
  accepts_nested_attributes_for :entry

  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00 AM'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date].strftime('%Y-%m-%d 23:59:59 PM'))
      where('date' => from_date..to_date)
    else
      all
    end
  end

  def create_interest_on_feeds_program
    accounts_receivables_trade = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")
    interest_income_from_credit_sales = Accounting::Account.find_by(name: "Interest Income from Credit Sales")
    line_items.each do |l|
      if l.stock.product.program.id == feeds_program.id
        program = l.stock.product.program
        interest = (program.interest_rate / 100) * l.total_price
        InterestProgram.create(line_item_id: l.id, amount: interest)
        Accounting::Entry.create(commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Interest for order ##{self.reference_number}.", 
        debit_amounts_attributes: [{amount: interest, account: accounts_receivables_trade}], 
        credit_amounts_attributes:[{amount: interest, account: interest_income_from_credit_sales}], 
        employee_id: self.employee_id)
      end
    end
  end

  def set_to_paid!
    if cash?
      line_items.each do |line_item|
        line_item.paid = true
      end
    end
  end

  def feeds_program
    Program.find_by(name: "Feeds")
  end

  def self.cost_of_goods_sold
    all.to_a.sum{ |a| a.cost_of_goods_sold }
  end

  def cost_of_goods_sold
    if service_charges.present?
      line_items.cost_of_goods_sold + purchases.sum(:total_cost) + labor_amount
    else
      line_items.cost_of_goods_sold
    end
  end

  def self.income
    all.to_a.sum{ |a| a.income }
  end

  def income
    if service_charges.present?
      total_amount_less_discount - cost_of_goods_sold
    else
      line_items.income - total_discount
    end
  end
  
  def customer_name
    customer.try(:full_name)
  end
  def vatable_amount
    0
  end
  def vat_percentage
    12
  end
  def machine_accreditation
    ""
  end
  def total_discount
    if discount.present?
      discount.amount
    else
      0
    end
  end
  def reference_number
    "#{id.to_s.rjust(8, '0')}"
  end
  def self.total_amount_without_discount
    all.map{|a| a.total_amount_without_discount }.sum
  end
  def self.total_amount_less_discount
    all.map{|a| a.total_amount_less_discount }.sum
  end
  def self.total_amount_less_discount_plus_interest
    all.map{|a| a.total_amount_less_discount }.sum + all.line_items.map {|l| l.interest_programs.sum(:amount)}.sum
  end
  def self.total_discount
    all.map{|a| a.total_discount }.sum
  end
  def tax_rate
    if business.non_vat_registered?
      0.03
    elsif business.vat_registered?
      0.12
    end
  end

  def self.retail_and_wholesale
    self.where.not(order_type: "catering").where.not(order_type: 'office_expense').includes(:customer, :invoice_number).order(date: :desc).all
  end

  def self.office_expenses
    self.where(order_type: "office_expense").includes(:customer, :invoice_number).order(date: :desc).all
  end
  
  def total_amount_without_discount
    if service_charges.present?
      total_catering_amount
    else
      line_items.sum(:total_price)
    end
  end

  def total_amount_of_consigned_items
    self.line_items.select { |l| l.stock.consigned == true}.pluck(:total_price).sum
  end

  def total_amount_less_consigned
    line_items.sum(:total_price) - total_amount_of_consigned_items
  end

  def total_amount_less_consigned_less_discount
    line_items.sum(:total_price) - total_amount_of_consigned_items - total_discount
  end

  def self.office_expenses_amount
    self.office_expense.sum {|o| o.line_items.sum(:total_price)}
  end

  def total_amount_with_discount
    total_amount_without_discount + total_discount
  end

  def total_interest
    #Accounting::Account.find_by_name('Interest Income from Credit Sales').credit_entries.pluck(:amount).sum
    line_items.sum {|l| l.interest_programs.sum(:amount)}
  end

  def total_payment
    line_items.where(paid: true).sum(:total_price) + line_items.where(paid: false).sum {|l| l.payments.sum(:amount)}
  end

  def total_amount_less_discount
    total_amount_without_discount - total_discount
  end

  def total_amount_less_discount_plus_interest
    total_amount_less_discount + total_interest
  end

  def balance
    total_amount_less_discount_plus_interest - total_payment
  end

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def add_additional_charges(cart)
    cart.service_charges.each do |item|
      item.cart_id = nil
      service_charges << item
    end
  end

  def purchases
    service_charges.where(charge_type: 1).all
  end

  def labor
    service_charges.where(charge_type: 0).all
  end

  def service_fee
    service_charges.where(charge_type: 2).all
  end

  def meals
    service_charges.where(charge_type: 3).all
  end

  def snacks
    service_charges.where(charge_type: 4).all
  end

  def meals_and_snacks
    meals + snacks
  end

  def line_items_amount
    line_items.sum(:total_price)
  end

  def total_expenses
    line_items_amount + purchases_amount + labor_amount
  end

  def purchases_amount
    purchases.sum(:total_cost)
  end

  def additional_charges
    labor + service_fee
  end

  def labor_amount
    labor.sum(:total_cost)
  end

  def service_fee_amount
    service_fee.sum(:total_cost)
  end

  def total_catering_amount
    meals.sum(:total_cost) + snacks.sum(:total_cost)
  end

  def stock_cost
    line_items.map{|a| a.stock.unit_cost * a.quantity}.sum
  end

  def return_line_items_to_stock!
    line_items do |line_item|
      line_item.stock.update_attributes!(quantity: line_item.quantity + line_item.stock.quantity)
    end
  end

  def remove_entry_for_return_order!
    Accounting::Entry.where(order_id: id).destroy_all
  end

  def subscribe_to_program!
    if self.line_items.last.stock.product.program.present?
      ProgramSubscription.create(customer_id: self.customer_id, program_id: self.line_items.last.stock.product.program.id)
    end
  end

  def set_customer_has_credit_to_true!
    Customer.find(customer_id).update(has_credit: true)
  end

  def set_has_credit_to_false!
    Customer.find(customer_id).update(has_credit: false)
  end

  def discontinue_stock_if_zero!
    line_items.each do |line_item|
      if line_item.stock.in_stock == 0
        line_item.stock.update!(stock_type: "discontinued")
      end
    end
  end
  def create_entry_for_program
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")
    @sales = Accounting::Account.find_by(name: "Sales")
    @cost_of_goods_sold = Accounting::Account.find_by(name: "Cost of Goods Sold")
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")

    entry = Accounting::Entry.credit_order.build(order_id: self.id, commercial_document: self.customer, date: self.date, description: "Credit",
    debit_amounts_attributes: [{amount: self.total_amount_without_discount, account_id: @accounts_receivable.id}, {amount: self.stock_cost, account_id: @cost_of_goods_sold.id}], 
    credit_amounts_attributes:[{amount: self.total_amount_without_discount, account_id: @sales.id}, {amount: self.stock_cost, account_id: @merchandise_inventory.id}], 
    employee_id: self.employee_id)
  end

  def any_consigned_items?
    self.line_items.select { |l| l.stock.consigned == true }.count != self.line_items.count
  end

  def all_consigned_items?
    self.line_items.select { |l| l.stock.consigned == true }.count == self.line_items.count
  end

  def zero_consigned_items?
    self.line_items.select { |l| l.stock.consigned == false }.count == self.line_items.count
  end

  def create_entry
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand - Teller")
    @purchases = Accounting::Account.find_by(name: "Purchases")
    @cost_of_goods_sold = Accounting::Account.find_by(name: "Cost of Goods Sold")
    @sales = Accounting::Account.find_by(name: "Sales")
    @consignment_sales = Accounting::Account.find_by(name: 'Consignment Sales')
    @catering_sales = Accounting::Account.find_by(name: "Catering Sales")
    @office_supplies = Accounting::Account.find_by(name: "Office Supplies")
    @salaries_and_wages = Accounting::Account.find_by(name: "Salaries and Wages")
    @service_fees = Accounting::Account.find_by(name: "Service Fees")
    @catering_expenses = Accounting::Account.find_by(name: 'Catering Purchases')
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")

    if self.cash? && !self.discounted? && !self.catering? && !self.office_expense?
      entry_for_cash

    elsif self.credit? && !self.discounted? && !self.catering? && !self.office_expense?
      entry_for_credit

    elsif self.cash? && self.discounted? && !self.catering? && !self.office_expense?
      entry_for_discounted_cash

    elsif self.credit? && self.discounted? && !self.catering? && !self.office_expense?
      entry_for_discounted_credit

    elsif self.credit? && !self.discounted? && !self.catering? && self.office_expense?
      entry_for_office_expenses

    elsif self.credit? && !self.discounted? && self.catering? && !self.office_expense?
      entry_for_catering
    end
  end

  def entry_for_catering
    Accounting::Entry.create(order_id: self.id, commercial_document_id: self.id, 
      commercial_document_type: self.class, date: self.date, 
      description: "Expenses for catering order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.purchases_amount, account: @catering_expenses}, 
                                {amount: self.labor_amount, account: @salaries_and_wages},
                                {amount: self.stock_cost, account: @cost_of_goods_sold}],
      credit_amounts_attributes:[{amount: self.purchases_amount, account: @cash_on_hand},
                                {amount: self.labor_amount, account: @cash_on_hand},
                                {amount: self.stock_cost, account: @merchandise_inventory}], 
      employee_id: self.employee_id)

    Accounting::Entry.create(entry_type: "credit_order", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Credit for catering order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.total_catering_amount, account: @accounts_receivable}],
      credit_amounts_attributes:[{amount: self.total_catering_amount, account: @catering_sales}], 
      employee_id: self.employee_id)
  end

  def entry_for_office_expenses
    Accounting::Entry.create(order_id: self.id, commercial_document_id: self.id, 
      commercial_document_type: self.class, date: self.date, 
      description: "#{self.name} ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.stock_cost, account: @office_supplies}],
      credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory}], 
      employee_id: self.employee_id)
  end

  def entry_for_cash
    if any_consigned_items?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Cash Sales of order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_without_discount, account: @cash_on_hand}], 
        credit_amounts_attributes: [{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_less_consigned, account: @sales},
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}],  
        employee_id: self.employee_id)
    elsif all_consigned_items?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Cash Sales of order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_of_consigned_items, account: @cash_on_hand}], 
        credit_amounts_attributes: [{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}],  
        employee_id: self.employee_id)
    elsif zero_consigned_items?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Cash Sales of order ##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.total_amount_without_discount, account: @cash_on_hand},
          {amount: self.stock_cost, account: @cost_of_goods_sold}], 
        credit_amounts_attributes:[{amount: self.total_amount_without_discount, account: @sales}, 
          {amount: self.stock_cost, account: @merchandise_inventory}],  
        employee_id: self.employee_id)
    end
  end

  def entry_for_credit
    if any_consigned_items?
      Accounting::Entry.create(entry_type: "credit_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit Sales of order##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_without_discount, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_less_consigned, account: @sales}, 
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}], 
        employee_id: self.employee_id)
    elsif all_consigned_items?
      Accounting::Entry.create(entry_type: "credit_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit Sales of order##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_of_consigned_items, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}], 
        employee_id: self.employee_id)
    elsif zero_consigned_items?
      Accounting::Entry.create(entry_type: "credit_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit Sales of order##{self.reference_number}", 
        debit_amounts_attributes: [{amount: self.total_amount_without_discount, account: @accounts_receivable}, 
          {amount: self.stock_cost, account: @cost_of_goods_sold}], 
        credit_amounts_attributes:[{amount: self.total_amount_without_discount, account: @sales}, 
          {amount: self.stock_cost, account: @merchandise_inventory}], 
        employee_id: self.employee_id)
    end
  end

  def entry_for_discounted_cash
    if any_consigned_items?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Cash Sales of order##{self.reference_number} with discount of #{self.total_discount}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_less_discount, account: @cash_on_hand}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory}, 
          {amount: self.total_amount_less_consigned_less_discount, account: @sales},
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}],  
        employee_id: self.employee_id)
    elsif all_consigned_items?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Cash Sales of order##{self.reference_number} with discount of #{self.total_discount}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_less_discount, account: @cash_on_hand}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory}, 
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}],  
        employee_id: self.employee_id)
    elsif zero_consigned_items?
      Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Cash Sales of order##{self.reference_number} with discount of #{self.total_discount}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_less_discount, account: @cash_on_hand}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory}, 
          {amount: self.total_amount_less_discount, account: @sales}],  
        employee_id: self.employee_id)
    end
  end

  def entry_for_discounted_credit
    if any_consigned_items?
      Accounting::Entry.create(order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit Sales of order ##{self.reference_number} with discount of #{self.total_discount}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_less_discount, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_less_consigned_less_discount, account: @sales},
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}],  
        employee_id: self.employee_id)
    elsif any_consigned_items?
      Accounting::Entry.create(order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit Sales of order ##{self.reference_number} with discount of #{self.total_discount}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_less_discount, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_of_consigned_items, account: @consignment_sales}],  
        employee_id: self.employee_id)
    elsif zero_consigned_items?
      Accounting::Entry.create(order_id: self.id, commercial_document_id: self.customer_id, 
        commercial_document_type: self.customer.class, date: self.date, 
        description: "Credit Sales of order ##{self.reference_number} with discount of #{self.total_discount}", 
        debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
          {amount: self.total_amount_less_discount, account: @accounts_receivable}], 
        credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
          {amount: self.total_amount_less_discount, account: @sales}],  
        employee_id: self.employee_id)
    end
  end

  private

  def ensure_not_referenced_by_line_items
    errors[:base] << "Order still referenced by line items" if self.line_items.present?
    return false 
  end

  def set_date
    self.date ||= Time.zone.now
  end

  def set_customer
    if customer_id.nil?
      customer_id = Customer.find_by(first_name: 'Guest').id
    end
  end
end
