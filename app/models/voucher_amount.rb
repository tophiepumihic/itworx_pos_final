class VoucherAmount < ApplicationRecord
  belongs_to :voucher
  belongs_to :voucher_session
  belongs_to :credit_account, class_name: 'Accounting::Account'
  belongs_to :debit_account, class_name: 'Accounting::Account'
  enum amount_type: [:debit, :credit]

  validates :amount, :description, :debit_account_id, :credit_account_id, :amount_type, presence: true
end
