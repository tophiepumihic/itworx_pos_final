class Voucher < ApplicationRecord
	has_many :voucher_amounts, dependent: :destroy
	belongs_to :employee, foreign_key: 'employee_id'
  before_save :default_date
  belongs_to :commercial_document, :polymorphic => true
  has_one :entry, class_name: "Accounting::Entry"

	validates :date, :description, presence: true

	def add_voucher_amounts_from_voucher_session(voucher_session)
    voucher_session.voucher_amounts.each do |voucher_amount|
      voucher_amount.voucher_session_id = nil
      voucher_amounts << voucher_amount
    end
  end
	
	private

	def default_date
		self.date ||= Time.zone.now
	end
end
