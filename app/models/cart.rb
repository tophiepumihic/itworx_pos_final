class Cart < ApplicationRecord
  belongs_to :employee
  has_many :line_items, dependent: :destroy
  has_many :service_charges, dependent: :destroy
  has_many :stocks, through: :line_items

  def purchases
    service_charges.where(charge_type: 1).all
  end

  def labor
    service_charges.where(charge_type: 0).all
  end

  def service_fee
    service_charges.where(charge_type: 2).all
  end

  def additional_charges
    labor + service_fee
  end

  def meals
    service_charges.where(charge_type: 3).all
  end

  def snacks
    service_charges.where(charge_type: 4).all
  end

  def meals_and_snacks
    meals + snacks
  end

  def catering_amount
    meals.sum(:total_cost) + snacks.sum(:total_cost)
  end

  def expenses_amount
    purchases.sum(:total_cost) + labor.sum(:total_cost) + service_fee.sum(:total_cost) + line_items_amount
  end

  def total_amount
    line_items.sum(:total_price)
  end

  def line_items_amount
    line_items.sum(:total_price)
  end

  def total_catering_cost
    catering_amount
  end

  def add_line_item(line_item)
    if self.stocks.include?(line_item.stock)
      self.line_items.where(stock_id: line_item.stock.id).delete_all
      # replace with a single item
      self.line_items.create!(program_id: line_item.stock.product.program.id, stock_id: line_item.stock.id, quantity: line_item.quantity, pricing_type: line_item.pricing_type, unit_price: line_item.unit_price, total_price: line_item.total_amount, user_id: self.employee_id)
    else
      self.line_items.create!(program_id: line_item.stock.product.program.id, stock_id: line_item.stock.id, quantity: line_item.quantity, pricing_type: line_item.pricing_type, unit_price: line_item.unit_price, total_price: line_item.total_amount, user_id: self.employee_id)
    end
  end
end
