class VoucherSessionsController < ApplicationController
	def show
    begin
    @voucher_session = VoucherSession.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error "Attempt to access invalid cart #{params[:id]}"
      redirect_to store_index_url, alert: 'The cart you were looking for could not be found.'
    else
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @voucher_session }
      end
    end
    @order = Order.new
    @order.build_discount
  end

  def voucher_form
  	@voucher_session = current_voucher_session
    @voucher_amount = VoucherAmount.new
    @voucher = Voucher.new
  end

  def destroy
    @voucher_session = current_voucher_session
    @voucher_session.destroy
    session[:voucher_session_id] = nil
    redirect_to vouchers_path, notice: "Voucher cancelled."
  end
end