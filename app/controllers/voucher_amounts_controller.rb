class VoucherAmountsController < ApplicationController

	def new
		@amount = VoucherAmount.new
	end

	def create
		@voucher_session = current_voucher_session
		@amount = @voucher_session.voucher_amounts.create(voucher_amount_params)
	end

	def destroy
		@voucher = current_voucher_session
		@amount = VoucherAmount.find(params[:id])
		@amount.destroy
		redirect_to voucher_form_voucher_sessions_path, notice: 'Amount deleted.'
	end

	private

	def voucher_amount_params
		params.require(:voucher_amount).permit(:amount, :description, :debit_account_id, :credit_account_id, :amount_type)
	end
end
		