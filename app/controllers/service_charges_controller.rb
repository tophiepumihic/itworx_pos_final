class ServiceChargesController < ApplicationController

  def new
    @cart = current_cart
    @service_charge = @cart.service_charges.build
  end

  def create
    @cart = current_cart
    @service_charge = @cart.service_charges.create(service_charge_params)
    @service_charge.compute_total_charges
    respond_to do |format|
      if @service_charge.save!
        @cart.add_service_charge(@service_charge)
        format.html { redirect_to caterings_url, notice: "Added to cart." }
        format.js { @current_item = @service_charge }
      else
        format.html { redirect_to caterings_url, notice: @service_charge.errors }
      end
    end
  end

  def destroy
    @service_charge = ServiceCharge.find(params[:id])
    @service_charge.destroy
    if request.referer == caterings_url
      redirect_to caterings_url, alert: "Item has been removed."
    elsif request.referer == order_path(@order)
      redirect_to order_path(@order), alert: "Item has been removed."
    end
  end

  private
  def service_charge_params
    params.require(:service_charge).permit(:description, :unit, :quantity, :unit_cost, :total_cost, :charge_type, :user_id)
  end
end
