module Customers
  class PaymentsController < ApplicationController
    def new
      @member = Customer.find(params[:customer_id])
      @entry = AccountsReceivablePaymentForm.new
      # authorize @entry
    end

    def create
      @member = Customer.find(params[:customer_id])
      @entry = AccountsReceivablePaymentForm.new(entry_params)
      if @entry.save
        @member.set_has_credit_to_false!
        redirect_to payments_customer_path(@member), notice: "Payment saved successfully."
      else
        render :new
      end
      # authorize @entry
    end

    def show
      @entry = Accounting::Entry.find(params[:id])
    end

    private
    def entry_params
      params.require(:accounts_receivable_payment_form).permit(:date, :description, :reference_number, :principal_amount, :interest_amount, :discount_amount, :commercial_document_id, :commercial_document_type, :employee_id)
    end
  end
end
