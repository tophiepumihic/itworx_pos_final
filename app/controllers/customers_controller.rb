class CustomersController < ApplicationController
  autocomplete :customer, :full_name, full: true

  def index
    if params[:full_name].present?
      @customers = Customer.search_by_name(params[:full_name]).page(params[:page]).per(30)
    else
      @customers = Customer.order(:id).all.page(params[:page]).per(30)
      @members = Member.with_cash_transactions.sort_by(&:annual_cash_transactions).reverse
      respond_to do |format|
        format.html
        format.pdf do
          pdf = CustomersPdf.new(@members, view_context)
                send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Customer Purchases Report.pdf"
        end
      end
    end
  end

  def import
    begin
      Member.import(params[:file])
      redirect_to settings_url, notice: 'Members Imported'
    rescue
      redirect_to settings_url, notice: 'Invalid CSV File.'
    end
  end

  def export
    @members = Member.order(:last_name).all
    respond_to do |format|
      format.xlsx { render xlsx: "export", disposition: 'inline', filename: "MembersExport-#{Time.zone.now.strftime('%B %e, %Y')}" }
    end
  end

  def autocomplete
    @members = Customer.all
    @names = @members.map { |m| m.full_name }
    render json: @names
  end

  def new
    @member = Customer.new
    @member.build_address
  end

  def create
    @members = Customer.all
    @member = Customer.create(customer_params)
  end

  def show
    @member = Customer.find(params[:id])
  end

  def info
    @member = Customer.find(params[:id])
  end

  def purchases
    @member = Customer.find(params[:id])
    if params[:from_date].present? && params[:to_date].present?
      @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
      @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
      @orders = @member.orders.created_between({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
      respond_to do |format|
        format.html
        format.pdf do
          pdf = Customers::LineItemsPdf.new(@member, @orders, @from_date, @to_date, view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Customer Purchases Report.pdf"
        end
      end
    else
      @cash_transactions = @member.orders.order(date: :desc).page(params[:page]).per(50)
      @credit_items = @member.credit_items
      @credit_orders = Kaminari.paginate_array(@credit_items).page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
          pdf = Customers::LineItemsPdf.new(@credit_orders, @from_date, @to_date, view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Customer Purchases Report.pdf"
        end
      end
    end
  end

  def payments
    @member = Customer.find(params[:id])
    if params[:from_date].present? && params[:to_date].present?
      @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
      @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
      @payment_entries = @member.payment_entries.order(:date).page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
           pdf = Customers::PaymentsPdf.new(@member, @from_date, @to_date, view_context)
                  send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Account Details Report.pdf"
        end
      end
    else
      @payment_entries = @member.payment_entries.page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
           pdf = Customers::PaymentsPdf.new(@member, view_context)
                  send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Account Details Report.pdf"
        end
      end
    end
  end

  def account_details
    @member = Customer.find(params[:id])
    if params[:from_date].present? && params[:to_date].present?
      @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
      @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
      @orders = @member.orders.credit.page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
           pdf = Customers::AccountDetailsPdf.new(@member, view_context)
                  send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Account Details Report.pdf"
        end
      end
    else
      @orders = @member.orders.credit.page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
           pdf = Customers::AccountDetailsPdf.new(@member, view_context)
                  send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Account Details Report.pdf"
        end
      end
    end
  end

  private
  def customer_params
    params.require(:customer).permit(:last_name, :first_name, :middle_name, :member_type, :type, :mobile, :profile_photo, address_attributes:[:id, :house_number, :street, :barangay, :municipality, :province, :id ])
  end
end
