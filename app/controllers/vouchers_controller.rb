class VouchersController < ApplicationController

	def index
		if params[:description].present?
      @entries = Accounting::Entry.voucher.search_by_name(params[:description]).order(date: :desc).page(params[:page]).per(30)
    else
			if (params[:from_date] && params[:to_date]).present?
        @from_date = params[:from_date] ? Time.parse(params[:from_date]) : Time.zone.now
        @to_date = params[:to_date] ? Time.parse(params[:to_date]) : Time.zone.now
        @voucher_entries = Accounting::Entry.voucher.created_between({from_date: @from_date, to_date: @to_date.end_of_day}).order(date: :desc)
				@entries = Kaminari.paginate_array(@voucher_entries).page(params[:page]).per(40)
        respond_to do |format|
          format.html
          format.pdf do
            pdf = EntriesPdf.new(@voucher_entries, @from_date, @to_date, view_context)
              send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Entries Report.pdf"
          end
        end
      else
        @voucher_entries = Accounting::Entry.voucher
				@entries = Kaminari.paginate_array(@voucher_entries).page(params[:page]).per(40)
        respond_to do |format|
          format.html
          format.pdf do
            pdf = EntriesPdf.new(@entries, @from_date, @to_date, view_context)
              send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Entries Report.pdf"
          end
        end
      end
    end
	end

	def scope_to_date
    @from_date = params[:from_date] ? Time.parse(params[:from_date]) : Time.zone.now
    @to_date = params[:to_date] ? Time.parse(params[:to_date]) : Time.zone.now
    @entries = Accounting::Entry.voucher.created_between({from_date: @from_date, to_date: @to_date.end_of_day}).order(date: :desc)
    respond_to do |format|
      format.html
      format.pdf do
        pdf = EntriesPdf.new(@entries, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Entries Report.pdf"
      end
    end
  end

	def new
		authorize :voucher
		@voucher_session = current_voucher_session
		@voucher = Voucher.new
	end

	def create
		@voucher = Voucher.create(voucher_params)
		@voucher.employee = current_user
    @voucher.add_voucher_amounts_from_voucher_session(current_voucher_session)
    if @voucher.save
      if @voucher.voucher_amounts.present?
				@voucher.voucher_amounts.all.each do |amount|
					Accounting::Entry.create!(date: @voucher.date, description: amount.description, 
						entry_type: 'voucher',
						employee_id: @voucher.employee_id, 
						debit_amounts_attributes: [{amount: amount.amount, account: Accounting::Account.find(amount.debit_account_id)}],
						credit_amounts_attributes: [{amount: amount.amount, account: Accounting::Account.find(amount.credit_account_id)}])
				end
			end
			VoucherSession.destroy(session[:voucher_session_id])
      session[:voucher_session_id] = nil
			redirect_to vouchers_path, notice: "Voucher saved."
    else
      @voucher_session = current_voucher_session
      render :new
    end
	end

	def disburse
		@voucher = current_voucher
		if @voucher.voucher_amounts.present?
			@voucher.voucher_amounts.all.each do |amount|
				Accounting::Entry.create!(date: @voucher.date, description: amount.description, 
					entry_type: 'voucher',
					employee_id: @voucher.employee_id, 
					debit_amounts_attributes: [{amount: amount.amount, account: Accounting::Account.find(amount.debit_account_id)}],
					credit_amounts_attributes: [{amount: amount.amount, account: Accounting::Account.find(amount.credit_account_id)}])
			end
		end
		@voucher.destroy
    session[:voucher_id] = nil
		redirect_to vouchers_path, notice: "Voucher saved."
	end

	def destroy
		@voucher.find(params[:id])
		@voucher.destroy
		redirect_to vouchers_path, notice: "Voucher deleted successfully."
	end

	private

	def voucher_params
		params.require(:voucher).permit(:date, :description, :employee_id)
	end
end