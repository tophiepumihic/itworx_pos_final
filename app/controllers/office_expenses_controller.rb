class OfficeExpensesController < ApplicationController
  
  def index
    @total_expenses_amount = Order.office_expenses_amount
    if params[:from_date].present? && params[:to_date].present?
      @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
      @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
      @expenses = Order.office_expense.created_between({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
      @office_expenses = Kaminari.paginate_array(@expenses).page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
          pdf = OfficeExpensesPdf.new(@office_expenses, @from_date, @to_date, view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Office Expenses Report.pdf"
        end
      end
    else
      @expenses = Order.office_expense
      @office_expenses = Kaminari.paginate_array(@expenses).page(params[:page]).per(50)
      respond_to do |format|
        format.html
        format.pdf do
          pdf = OfficeExpensesPdf.new(@office_expenses, @from_date, @to_date, view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Office Expenses Report.pdf"
        end
      end
    end
  end

  def scope_to_date
    @office_expenses = Order.office_expense.order(date: :desc).all
    @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
    @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
    respond_to do |format|
      format.html
      format.pdf do
        pdf = OfficeExpensesPdf.new(@office_expenses, @from_date, @to_date, view_context)
        send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Member's Purchases.pdf"
        pdf.print
      end
    end
  end

  def new
    if params[:name_and_description].present?
      @products = Product.search_by_name(params[:name_and_description]).pluck(:id)
      if @products.present?
        @stocks = Stock.order(date: :asc).joins(:product).where(products: { id: @products })
      else
        redirect_to new_office_expense_path, notice: 'Product is either out of stock or expired.'
      end
    else
      @stocks = Stock.purchased.all
    end
    authorize :office_expense
    @cart = current_cart
    @line_item = LineItem.new
    @order = Order.new
    @order.build_discount
  end
end