class ProgramsController < ApplicationController

  def new
    @program = Program.new
  end

  def create
    @programs = Program.all
    @program = Program.create(program_params)
  end

  def edit
    @program = Program.find(params[:id])
  end

  def update
    @program = Program.find(params[:id])
    @program.update_attributes(program_params)
  end

  def destroy
    @program = Program.find(params[:id])
    @program.destroy
    redirect_to settings_path, notice: 'Interest Program has been removed.'
  end

  private
  def program_params
    params.require(:program).permit(:name, :interest_rate, :number_of_days, :accounts_receivable_account_id, :accounts_receivable_interest_account_id)
  end
end
