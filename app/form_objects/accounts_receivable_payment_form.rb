class AccountsReceivablePaymentForm
	include ActiveModel::Model 
	attr_accessor :discount_amount, :interest_amount, :principal_amount, :date, :description, :employee_id, :reference_number, :commercial_document_id, :commercial_document_type
  validates :interest_amount, :principal_amount, :discount_amount, numericality: true
  
  def save 
    ActiveRecord::Base.transaction do
      set_items_to_paid!
      save_payment
    end
  end

  private
    def save_payment
      principal_account = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")
      cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand - Teller")
      interest_account = Accounting::Account.find_by(name: 'Interest Income from Credit Sales')
      discount_account = Accounting::Account.find_by(name: 'Interest Discount for Credit Sales')
      total = principal_amount.to_f + (interest_amount.to_f - discount_amount.to_f)
      interest_less_discount = interest_amount.to_f - discount_amount.to_f

    	Accounting::Entry.credit_payment.create!(date: date, reference_number: reference_number, 
    		commercial_document_type: commercial_document_type, employee_id: employee_id,
    		commercial_document_id: commercial_document_id, description: description,
    		debit_amounts_attributes: [{amount: principal_amount, account: cash_on_hand},
    			{amount: interest_amount, account: cash_on_hand}],
    		credit_amounts_attributes: [{amount: principal_amount, account: principal_account}, 
    			{amount: interest_less_discount, account: interest_account},
    			{amount: discount_amount, account: discount_account}])
    end

    # def set_items_to_paid!
    #   new_payment = principal_amount.to_f + (interest_amount.to_f - discount_amount.to_f)
    #   line_items = Customer.find(commercial_document_id).unpaid_items
    #   line_items.each do |l|
    #     total_payments = l.payments.sum(:amount)
    #     item_balance = l.total_price - total_payments
    #     balance = item_balance - new_payment
    #     if balance == 0
    #       l.update(paid: true)
    #       return false
    #     elsif balance < 0
    #       l.update(paid: true)
    #       new_payment = balance.abs
    #     elsif balance > 0
    #       l.payment.create(amount: balance)
    #       return false
    #     end          
    #   end
    # end
    def set_items_to_paid!
      new_payment = principal_amount.to_f + (interest_amount.to_f - discount_amount.to_f)
      line_items = Customer.find(commercial_document_id).unpaid_items
      line_items.each do |l|
        total_payments = l.payments.sum(:amount)
        item_balance = l.total_price - total_payments
        new_payment -= item_balance
        if new_payment == 0
          l.update(paid: true)
          return false
        elsif new_payment < 0
          Payment.create(amount: item_balance+new_payment, line_item_id: l.id)
          return false
        elsif new_payment > 0
          l.update(paid: true)
        end          
      end
    end
end